package com.wipro.springtest.service;

import org.springframework.stereotype.Service;

@Service
public class GreetService {
	
	public String greet()
	{
		return "Hello World";
	}

}
