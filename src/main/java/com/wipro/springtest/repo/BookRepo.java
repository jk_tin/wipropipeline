package com.wipro.springtest.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.springtest.dto.Book;

public interface BookRepo extends JpaRepository<Book, Long> {

}
