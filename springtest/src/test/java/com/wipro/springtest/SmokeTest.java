package com.wipro.springtest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.wipro.springtest.controller.HomeController;

@SpringBootTest
public class SmokeTest {
	@Autowired
	HomeController homeController;
	
	@Test
	void homeLoaded()
	{
		assertThat(homeController).isNotNull();
	}

}
