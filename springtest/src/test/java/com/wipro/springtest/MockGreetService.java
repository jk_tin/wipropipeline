package com.wipro.springtest;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.hamcrest.Matchers.containsString;
import com.wipro.springtest.controller.*;
import com.wipro.springtest.service.GreetService;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@WebMvcTest(HomeController.class)
public class MockGreetService {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private GreetService service;

	@Test
	void greetingShouldReturnMessageFromService() throws Exception {
		when(service.greet()).thenReturn("Hello, Mock");
		this
		.mockMvc
		.perform(get("/hello"))
		.andDo(print())
		.andExpect(status().isOk())
				.andExpect(content()
				.string(containsString("Hello, Mock")));
	}

}
