package com.wipro.springtest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.wipro.springtest.repo.BookRepo;
import com.wipro.springtest.dto.*;
@DataJpaTest
public class BookRepoTest {
	@Autowired
	private TestEntityManager testEM;
	
	@Autowired
	private BookRepo bookRepo;
	
	@Test
	  public void testSave() {

	      Book b1 = new Book("Book A"       
	              );

	      //testEM.persistAndFlush(b1); the same
	      bookRepo.save(b1);

	      Long savedBookID = b1.getId();

	      Book book = bookRepo.findById(savedBookID).orElseThrow();
	      // Book book = testEM.find(Book.class, savedBookID);

	      assertEquals(savedBookID, book.getId());
	      assertEquals("Book A", book.getTitle());
	}
}
