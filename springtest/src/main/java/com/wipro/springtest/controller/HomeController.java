package com.wipro.springtest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.springtest.service.GreetService;

@RestController
public class HomeController {

	GreetService greetService;
	
	
	@GetMapping("/greet")
	public String greet()
	{
		return "Hello World!";
		
	}
	
	@GetMapping("/hello")
	public String hello()
	{
		return greetService.greet() ;
		
	}

	public HomeController(GreetService greetService) {
		super();
		this.greetService = greetService;
	}
	
	
	
}
